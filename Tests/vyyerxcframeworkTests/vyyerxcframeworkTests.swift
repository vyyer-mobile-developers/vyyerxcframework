import XCTest
@testable import vyyerxcframework

final class vyyerxcframeworkTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(vyyerxcframework().text, "Hello, World!")
    }
}
